# What is this?

This is some ansible playbooks to make it easier to get to a known clean state for testing various parts of AssemblyLine.

It also configures ``~/.ssh/config`` with sane host name aliases

You should be able to use these playbooks by:
 
1. Clone this repo
2. ``sudo pip install ansible awscli boto boto3``
3. Set up aws CLI with your AWS key
4. Modify `group_vars/all.yaml` as appropriate (or overwrite variables at command line) 
5. Run the playbook with ``ansible-playbook -v aldev.yaml``
